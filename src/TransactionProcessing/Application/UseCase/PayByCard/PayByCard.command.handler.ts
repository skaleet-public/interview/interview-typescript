import {TransactionRepository} from "../../../Domain/Gateway/Transaction.repository";
import {AccountRepository} from "../../../Domain/Gateway/Account.repository";

export interface PayByCardCommand {
    clientAccountNumber: string,
    merchantAccountNumber: string,
    amount: number,
    currency: string,

}

export class PayByCardCommandHandler {
    constructor(
        private readonly transactionRepository: TransactionRepository,
        private readonly accountRepository: AccountRepository,
    ) {
    }

    public handle(command: PayByCardCommand) {
        // write your code here

        // Here are the rules to implement:
        // - The input amount is strictly positive.
        // - The currency of the impacted accounts and of the payment must be identical.
        // - The customer's account is debited by the amount of the transaction.
        // - The merchant's account is credited with the transaction amount.
        // - The transaction date is the current date at the time of payment.
    }
}
