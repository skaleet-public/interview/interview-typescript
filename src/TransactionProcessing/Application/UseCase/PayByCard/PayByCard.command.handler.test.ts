import {Account} from "../../../Domain/Model/Account";
import {Amount} from "../../../Domain/Model/Amount";
import {InMemoryDataBase} from "../../../Infrastructure/Gateway/InMemoryDataBase";
import {PayByCardCommandHandler} from "./PayByCard.command.handler";
import {TransactionRepository} from "../../../Domain/Gateway/Transaction.repository";
import {AccountRepository} from "../../../Domain/Gateway/Account.repository";
import {TransactionLog} from "../../../Domain/Model/TransactionLog";
import {AccountingEntry} from "../../../Domain/Model/AccountingEntry";

describe('Exercice #1', () => {
    // These constants hold valid account numbers that you can use in the test cases
    const BANK_EUR = '847596';
    const BANK_USD = '145896';
    const MERCHANT_EUR = '371954';
    const MERCHANT_USD = '284619';
    const CLIENT_EUR = '951575';
    const CLIENT_USD = '745288';

    let payByCardCommandHandler: PayByCardCommandHandler;
    let transactionRepository: TransactionRepository;
    let accountRepository: AccountRepository;

    beforeEach(() => {
        // This database is populated with some accounts and transactions
        const database = new InMemoryDataBase([
            new Account(BANK_EUR, new Amount(-2150_00, 'EUR')),
            new Account(MERCHANT_EUR, new Amount(2000_00, 'EUR')),
            new Account(CLIENT_EUR, new Amount(150_00, 'EUR')),
            new Account(BANK_USD, new Amount(-1825_00, 'USD')),
            new Account(MERCHANT_USD, new Amount(1750_00, 'USD')),
            new Account(CLIENT_USD, new Amount(75_00, 'USD')),
        ], [
            new TransactionLog('abcd', new Date('2024/01/15 11:14:42'), [
                new AccountingEntry(BANK_EUR, new Amount(-2000_00, 'EUR'), new Amount(-2000_00, 'EUR')),
                new AccountingEntry(MERCHANT_EUR, new Amount(2000_00, 'EUR'), new Amount(2000_00, 'EUR')),
            ]),
            new TransactionLog('efgh', new Date('2024/01/15 13:45:22'), [
                new AccountingEntry(BANK_EUR, new Amount(-150_00, 'EUR'), new Amount(-2150_00, 'EUR')),
                new AccountingEntry(CLIENT_EUR, new Amount(150_00, 'EUR'), new Amount(150_00, 'EUR')),
            ]),
            new TransactionLog('ijkl', new Date('2024/01/15 14:33:12'), [
                new AccountingEntry(BANK_USD, new Amount(-1750_00, 'USD'), new Amount(-1750_00, 'USD')),
                new AccountingEntry(MERCHANT_USD, new Amount(1750_00, 'USD'), new Amount(1750_00, 'USD')),
            ]),
            new TransactionLog('mnop', new Date('2024/01/15 15:12:34'), [
                new AccountingEntry(BANK_USD, new Amount(-75_00, 'USD'), new Amount(-1825_00, 'USD')),
                new AccountingEntry(CLIENT_USD, new Amount(75_00, 'USD'), new Amount(75_00, 'USD')),
            ]),
        ]);
        transactionRepository = database;
        accountRepository = database;
        payByCardCommandHandler = new PayByCardCommandHandler(transactionRepository, accountRepository);
    });

    describe('PayByCardCommandHandlerTest', () => {

        it('write your test here', () => {

            // this runs the pay by card command
            payByCardCommandHandler.handle({
                clientAccountNumber: CLIENT_EUR,
                merchantAccountNumber: MERCHANT_EUR,
                amount: 50_00,
                currency: 'EUR',
            });

            // this is dummy assertion, it needs to be replaced with something relevant
            expect(null).toBeNull();
        });
    });
});
