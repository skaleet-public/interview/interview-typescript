import {AccountingEntry} from "./AccountingEntry";

export class TransactionLog {
    constructor(
        public id: string,
        public date: Date,
        public accounting: AccountingEntry[],
    ) {
    }
}
