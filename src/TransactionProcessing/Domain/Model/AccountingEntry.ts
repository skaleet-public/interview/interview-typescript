import {Amount} from "./Amount";

export class AccountingEntry {
    constructor(
        public accountNumber: string,
        public amount: Amount,
        public newBalance: Amount,
    ) {
    }
}
